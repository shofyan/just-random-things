#!/bin/bash

if [[ -z "${SSM_PATH}" ]]; then
    echo "No SSM PATH given"
    exit 1
fi

SSM_PARAM_LIST=$(aws ssm get-parameters-by-path --path "${SSM_PATH}" --query "Parameters[*].Name" --output text)
jq -n --arg param_list "${SSM_PARAM_LIST}" '{"param_list":$param_list}'

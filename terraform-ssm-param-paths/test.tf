locals {
  ssm_params = split("\t", data.external.config.result.param_list)
  ssm_path   = "/ea-integration/cba/sftp/dev/"
}
data external config {
  program = ["bash", "./get-ssm-params.sh"]
  query = {
    SSM_PATH = local.ssm_path
  }
}

data "aws_ssm_parameter" "params" {
  for_each = {
    for item in local.ssm_params : item => item
  }
  name = each.key
}

output test {
  value = data.aws_ssm_parameter.params
}